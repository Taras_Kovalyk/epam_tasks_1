﻿using System;
using System.Text;

namespace Task_3
{
    /// <summary>
    /// Type that is the sum of several terms that contain different powers of the same variable(s)
    /// </summary>
    public class Polynomial
    {
        public double[] Coefficients { get; private set; }
        public int Power { get; private set; }
        public Polynomial(int power, double[] coefficients)
        {
            if (power > coefficients.Length - 1)
            {
                Coefficients = coefficients;
                Power = Coefficients.Length - 1;
            }
            else
            {
                if (power < coefficients.Length - 1)
                {

                    Coefficients = new double[power + 1];
                    Array.Copy(coefficients, Coefficients, power + 1);
                }
                else
                {
                    Coefficients = coefficients;
                }
                Power = power;
            }
        }
        private static Polynomial PerformOperation(Polynomial morePowerPolynomial, Polynomial lessPowerPolynomial, Func<double, double, double> function)
        {
            var resultCoefficients = new double[morePowerPolynomial.Power + 1];
            int morePower = morePowerPolynomial.Power;
            int lessPower = lessPowerPolynomial.Power;

            for (int i = 0, j = 0; i < resultCoefficients.Length; i++)
            {
                if (morePower > lessPower)
                    resultCoefficients[i] = morePowerPolynomial.Coefficients[i];
                else
                {
                    resultCoefficients[i] = function(morePowerPolynomial.Coefficients[i], lessPowerPolynomial.Coefficients[j]);
                    j++;
                }
                morePower--;
            }

            return new Polynomial(morePowerPolynomial.Power, resultCoefficients);
        }
        private static Polynomial ChangeCoefficientsSigns(Polynomial polynomial)
        {
            var resultCoefficients = new double[polynomial.Power + 1];
            for (int i = 0; i <= polynomial.Power; i++)
            {
                resultCoefficients[i] = polynomial.Coefficients[i] * (-1);
            }

            return new Polynomial(polynomial.Power, resultCoefficients);
        }
        public static Polynomial operator +(Polynomial left, Polynomial right)
        {
            if (left.Power > right.Power)
            {
                return PerformOperation(left, right, (x, y) => x + y);
            }

            return PerformOperation(right, left, (x, y) => x + y);
        }
        public static Polynomial operator -(Polynomial left, Polynomial right)
        {
            if (left.Power > right.Power)
            {
                return PerformOperation(left, right, (x, y) => x - y);
            }

            return PerformOperation(ChangeCoefficientsSigns(right), left, (x, y) => x + y);
        }

        public static Polynomial operator *(Polynomial left, Polynomial right)
        {
            var resultCoefficients = new double[left.Power + right.Power + 1];
            for (int i = 0; i <= left.Power; i++)
            {
                for (int j = 0; j <= right.Power; j++)
                {
                    resultCoefficients[i + j] += left.Coefficients[i] * right.Coefficients[j];
                }
            }

            return new Polynomial(left.Power + right.Power, resultCoefficients);
        }

        /// <summary>
        /// Calculating the value of the polynomial for a given argument
        /// </summary>
        /// <param name="argument"></param>
        /// <returns></returns>
        public double Calculate(double argument)
        {
            double result = 0;
            int power = 0;
            for (int i = Coefficients.Length - 1; i >= 0; i--)
            {
                if (power == 0)
                    result += Coefficients[i];
                else
                {
                    result += Coefficients[i] * argument;
                    argument *= argument;
                }
                power++;
            }
            return result;
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            int power = Power;
            for (int i = 0; i < Coefficients.Length; i++)
            {
                if (Coefficients[i] != 0)
                    if (power > 0)
                    {
                        if (Coefficients[i + 1] >= 0)
                            result.Append(Coefficients[i] + "*X^" + power + " +");
                        else
                            result.Append(Coefficients[i] + "*X^" + power + " ");
                        power--;
                    }
                    else
                        result.Append(Coefficients[i]);
                else
                    power--;
            }
            return result.ToString();
        }
    }
}
