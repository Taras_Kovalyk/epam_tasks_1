﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("First matrix");
            var firstMatrix = new Matrix(new double[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 2, 3, 12 }, { 13, 14, 1, 16 } });
            Console.WriteLine(firstMatrix);

            Console.WriteLine("Second matrix");
            var secondMatrix = new Matrix(new double[,] { { 4, 1, 2, 4 }, { 7, 7, 6, 15 }, { 2, 4, 7, 13 }, { 3, 4, 7, 15 } });
            Console.WriteLine(secondMatrix);

            Console.WriteLine($"Determinant of first matrix: {firstMatrix.GetDeterminant()}");
            Console.WriteLine($"Determinant of second matrix: {secondMatrix.GetDeterminant()}");
            Console.WriteLine();

            Console.WriteLine("Sum:");
            Console.WriteLine(firstMatrix + secondMatrix);

            Console.WriteLine("Difference");
            Console.WriteLine(firstMatrix - secondMatrix);

            Console.WriteLine("Product");
            Console.WriteLine(firstMatrix * secondMatrix);

            Console.WriteLine("Product of nonsquare matrices");
            var thirdMatrix = new Matrix(new double[,] { { 1, 2, 3 }, { 4, 5, 6 } });
            var fourthMatrix = new Matrix(new double[,] { { 1, 2 }, { 3, 4 }, { 5, 6 } });

            Console.WriteLine("Third matrix:");
            Console.WriteLine(thirdMatrix);

            Console.WriteLine("Fourth matrix");
            Console.WriteLine(fourthMatrix);

            Console.WriteLine("Product:");
            Console.WriteLine(thirdMatrix * fourthMatrix);
            

            Console.WriteLine($"(2,3)-Minor of first matrix: {firstMatrix.FindMinorij(2, 3)}");
            Console.WriteLine();

            try
            {
                Console.WriteLine("Trying to find (4,2)-Minor of second matrix:");
                Console.WriteLine(secondMatrix.FindMinorij(4, 2));
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine();
            }

            Console.WriteLine($"Minor of k = 2 order of first matrix: {firstMatrix.FindFirstMinorOfOrder(2)}" + Environment.NewLine);
            try
            {
                Console.WriteLine("Trying to find minor of k = 7 order of first matrix");
                Console.WriteLine(firstMatrix.FindFirstMinorOfOrder(7));
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine();
            }

            Console.WriteLine($"Complement minor of order k = 3 of first matrix {firstMatrix.FindComplementMinorOfOrder(3)}" + Environment.NewLine);
            try
            {
                Console.WriteLine("Trying to find complement minor of order k = 2 of first matrix");
                Console.WriteLine(firstMatrix.FindComplementMinorOfOrder(2));
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
