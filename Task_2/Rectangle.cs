﻿using System;

namespace Task_2
{
    public class Rectangle
    {
        public Point LeftBottomPoint { get; private set; }
        public Point LeftTopPoint { get; private set; }
        public Point RightTopPoint { get; private set; }
        public Point RightBottomPoint { get; private set; }
        public double Width { get; private set; }
        public double Heigth { get; private set; }
        public Rectangle(Point leftBottomPoint, double width, double heigth)
        {
            LeftBottomPoint = leftBottomPoint;
            LeftTopPoint = new Point(leftBottomPoint.X, leftBottomPoint.Y + heigth);
            RightTopPoint = new Point(leftBottomPoint.X + width, leftBottomPoint.Y + heigth);
            RightBottomPoint = new Point(leftBottomPoint.X + width, leftBottomPoint.Y);
            Width = width;
            Heigth = heigth;
        }

        /// <summary>
        /// Static method that provides a possibility to get Rectangle which is an intersection of first and second rectangles
        /// </summary>
        /// <param name="firstRectangle"></param>
        /// <param name="secondRectangle"></param>
        /// <returns></returns>
        public static Rectangle Intersect(Rectangle firstRectangle, Rectangle secondRectangle)
        {
            double newLeftBottomX = Math.Max(firstRectangle.LeftBottomPoint.X, secondRectangle.LeftBottomPoint.X);
            double newLeftBottomY = Math.Max(firstRectangle.LeftBottomPoint.Y, secondRectangle.LeftBottomPoint.Y);
            double newRightTopX = Math.Min(firstRectangle.RightTopPoint.X, secondRectangle.RightTopPoint.X);
            double newRightTopY = Math.Min(firstRectangle.RightTopPoint.Y, secondRectangle.RightTopPoint.Y);

            double width = newRightTopX - newLeftBottomX;
            double height = newRightTopY - newLeftBottomY;

            if (newLeftBottomX < newRightTopX && newLeftBottomY < newRightTopY)
                return new Rectangle(new Point(newLeftBottomX, newLeftBottomY), width, height);

            throw new InvalidOperationException("There is no rectangle in intersection");
        }

        /// <summary>
        /// Static method that provides a possibility to get Rectangle which is the smallest union of first and second rectangles
        /// </summary>
        /// <param name="rectangle1"></param>
        /// <param name="rectangle2"></param>
        /// <returns></returns>
        public static Rectangle MinUnion(Rectangle rectangle1, Rectangle rectangle2)
        {
            double newLeftBottomX = Math.Min(rectangle1.LeftBottomPoint.X, rectangle2.LeftBottomPoint.X);
            double newLeftBottomY = Math.Min(rectangle1.LeftBottomPoint.Y, rectangle2.LeftBottomPoint.Y);
            double newRightTopX = Math.Max(rectangle1.RightTopPoint.X, rectangle2.RightTopPoint.X);
            double newRightTopY = Math.Max(rectangle1.RightTopPoint.Y, rectangle2.RightTopPoint.Y);
            double width = newRightTopX - newLeftBottomX;
            double height = newRightTopY - newLeftBottomY;
            return new Rectangle(new Point(newLeftBottomX, newLeftBottomY), width, height);
        }

        /// <summary>
        /// Resizing the rectangle
        /// </summary>
        /// <param name="width"></param>
        /// <param name="heigth"></param>
        public void Resize(double width, double heigth)
        {
            LeftTopPoint = new Point(LeftBottomPoint.X, LeftBottomPoint.Y + heigth);
            RightTopPoint = new Point(LeftBottomPoint.X + width, LeftBottomPoint.Y + heigth);
            RightBottomPoint = new Point(LeftBottomPoint.X + width, LeftBottomPoint.Y);
        }

        /// <summary>
        /// Moving the rectangle 
        /// </summary>
        /// <param name="dX"></param>
        /// <param name="dY"></param>
        public void Move(double dX, double dY)
        {
            LeftBottomPoint = new Point(LeftBottomPoint.X + dX, LeftBottomPoint.Y + dY);
            LeftTopPoint = new Point(LeftTopPoint.X + dX, LeftTopPoint.Y + dY);
            RightTopPoint = new Point(RightTopPoint.X + dX, RightTopPoint.Y + dY);
            RightBottomPoint = new Point(RightBottomPoint.X + dX, RightBottomPoint.Y + dY);
        }

        public override string ToString()
        {
            return LeftBottomPoint + " " + LeftTopPoint + " " + RightTopPoint + " " + RightBottomPoint;
        }
    }
}
